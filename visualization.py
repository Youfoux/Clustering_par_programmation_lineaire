import warnings
import pandas as pd
import matplotlib.pyplot as plt
import sys
import subprocess
from sklearn.datasets.samples_generator import make_blobs
from mpl_toolkits.mplot3d import Axes3D


def main(nb_point, dim, nb_cluster, input_file, output_file, generate=True):

    warnings.filterwarnings('ignore')

    data_file = input_file

    if generate:
        # Generate a blob dataset
        x, y = make_blobs(n_samples=int(nb_point), n_features=int(dim), centers=int(nb_cluster))
        with open('generate_blobs.txt', 'w') as f:
            for value in x:
                str_write = str(value[0]) + ' ' + str(value[1]) + '\n'
                f.write(str_write)
        data_file = 'generate_blobs.txt'
        # subprocess.call(["./generation", nb_point, dim])

    subprocess.call(["./clustering", nb_point, nb_cluster, dim, data_file, 'temp'])

    # Read created Clustering
    file = 'temp'
    with open(file) as f:
        content = f.readlines()
    content = [x.strip() for x in content]

    # Create the Dataframe
    df = pd.DataFrame(numb.split(' ') for numb in content)

    for column in df.columns:
        df[column] = df[column].astype(float)

    # Separate the cluster
    list_cluster = []
    for center in df[int(dim)].unique():
        list_cluster.append(df[df[int(dim)] == center])

    # Plot the cluster
    fig = plt.figure(figsize=(15, 9))
    ax = fig.add_subplot(111, projection='3d')

    for index, cluster in enumerate(list_cluster):
        ax.scatter(cluster[0], cluster[1], cluster[2])
        cluster_x = cluster.loc[int(cluster[int(dim)].unique()) - 1][0]
        cluster_y = cluster.loc[int(cluster[int(dim)].unique()) - 1][1]
        cluster_z = cluster.loc[int(cluster[int(dim)].unique()) - 1][2]
        ax.scatter(cluster_x, cluster_y, cluster_z, color='black')
        ax.text(cluster_x + cluster[0].max() / 100, cluster_y + cluster[1].max() / 100,
                cluster_z + cluster[2].max() / 100, 'C_' + str(index + 1))
        ax.grid(True)
    plt.savefig(output_file)
    return 0


if __name__ == '__main__' and len(sys.argv) > 6:
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])
