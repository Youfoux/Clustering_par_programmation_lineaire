#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int main(int argc, char **argv)
{
	FILE * fichier = NULL;
	FILE * sortie = NULL;
	int nbPoint;
	
	
	if(argc > 4)
	{
		
		fichier =fopen(argv[2],"r");
		nbPoint = atoi(argv[3]);
		int totClus;
		int tableau [nbPoint];
		int k;
		
		for(k=0;k<nbPoint;k++)
		{
			tableau[k] = 0;
		}
		
		int curClus = 1;
		char courChar;
		int j;
		int index;
		fseek(fichier,10,SEEK_CUR);
		for(j = 0; j< nbPoint;j++)
		{
			courChar = fgetc(fichier);
	
			if(courChar != 'c')
			{
				fseek(fichier,-1,SEEK_CUR);
				fscanf(fichier,"%d",&index);
				fseek(fichier,1,SEEK_CUR);
				tableau[index-1] = curClus;
			}
			else
			{
				curClus ++;
				fseek(fichier,9,SEEK_CUR);
			}		
			
		}
		
		
		totClus = curClus;
		
		
		for(j = 0;j<totClus;j++)
		{
			while(courChar != ':')
			{
				courChar = fgetc(fichier);
			}
			fseek(fichier,1,SEEK_CUR);
			fscanf(fichier,"%d",&index);
			tableau[index-1] = -1 * (j+1);
			courChar =fgetc(fichier);			
		}
		
		fclose(fichier);
		
		fichier = fopen(argv[1],"r");
		sortie = fopen(argv[4],"w");
		
		char string[100];
		
		for(j=0; j<nbPoint; j++)
		{
			fgets(string,100,fichier);
			printf("string %s",string);
			fflush(stdout);
			fprintf(sortie,"%s",string);
			fseek(sortie,-1,SEEK_CUR);
			fprintf(sortie,"%d\n",tableau[j]);
		}
		fclose(sortie);
		/*for(j =0;j<nbPoint;j++)
		{
				printf("%d : %d \n",j+1,tableau[j]);
		}*/
										
	}	
}
