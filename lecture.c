#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char** argv) {
	int i,j;
	int k;
	int nb = 0;
	int dim = 2;
	int h = 0;
	float distance,somme;
	printf("nombre de points?");
	scanf("%d",&nb);
	printf("nombre de clusters?");
	scanf("%d", &h);
	float tableau[nb][dim];
	FILE* fichier = NULL;
	
	if(argc>0)
		fichier = fopen(argv[1], "r");
		
		
	for (i=0;i<nb;i++) {
		for (j=0;j<dim;j++) {
			fscanf(fichier, "%f ", &tableau[i][j]);
		}
	}
	fclose(fichier);
	fichier = fopen("distances.mod", "w");
	fprintf(fichier, "param n;\nparam h;\nparam B;\nparam D{i in 1..n,j in 1..n};\nvar M{i in 1..h,j in 1..n,k in 1..n}binary;\nvar C{i in 1..h,j in 1..n}binary;\nsubject to C1{i in 1..h}:\nsum {j in 1..n} C[i,j] = 1;\nsubject to C2{j in 1..n}:\nsum {i in 1..h} C[i,j] <= 1;\nsubject to C3{i in 1..h}:\nsum {j in 1..n} sum{k in 1..n} M[i,j,k] >= 1;\nsubject to C4{k in 1..n}:\nsum {i in 1..h} sum{j in 1..n} M[i,j,k] = 1;\nsubject to C5{i in 1..h,j in 1..n}:\nsum {m in 1..n: m!=j} sum{k in 1..n} M[i,m,k] <= (1-C[i,j])*B;\nminimize f:\n sum{i in 1..h,j in 1..n,k in 1..n} M[i,j,k]*D[j,k];\nsolve;\ndisplay M;\ndisplay C;\ndisplay f;\ndata;\n");
	fprintf(fichier,"param n := %d;\n", nb);
	fprintf(fichier,"param h := %d;\n", h);
	fprintf(fichier,"param B := %d;\n", 2*nb);
	fprintf(fichier,"param D :=\n");
	for (i=0;i<nb;i++) {
		for (k=0;k<nb;k++) {
			distance = 0;
			somme = 0;
			for (j=0;j<dim;j++) {
				somme += pow((tableau[i][j] - tableau[k][j]),2);
			}
			distance = sqrt(somme);
			fprintf(fichier, "%d %d %f\n", i+1, k+1, distance);
		}
	}
	fprintf(fichier,";\nend;\n");
	fclose(fichier);
	
	return 0;
}
