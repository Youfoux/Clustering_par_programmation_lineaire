param n;
param h;
param B;
param D{i in 1..n,j in 1..n};

var M{i in 1..h,j in 1..n,k in 1..n} binary;
var C{i in 1..h,j in 1..n}binary;

subject to C1{i in 1..h}:
	sum {j in 1..n} C[i,j] = 1; #chaque classe a un seul centre 
	
subject to C2{j in 1..n}:
	sum {i in 1..h} C[i,j] <= 1; #1 point n est pas centre de plusieurs classes
	
subject to C3{i in 1..h}:
	sum {j in 1..n} sum{k in 1..n} M[i,j,k] >= 1; # pas de classe vide
	
subject to C4{k in 1..n}:
	sum {i in 1..h} sum{j in 1..n} M[i,j,k] = 1; # 1point appartien uniquement a une classe

subject to C5{i in 1..h,j in 1..n}:
	sum {m in 1..n: m!=j} sum{k in 1..n} M[i,m,k] <= (1-C[i,j])*B; 

	
#subject to C5{i in 1..n, k in 1..K}:
	#C[i,k] <= M[i,k];
	
#appartenance de deux points a une classe
#subject to C6{i in 1..h}:
#sum {m in 1..n} M[i,m,1] = sum {m in 1..n} M[i,m,2]; 

#non appartenance de deux points au meme cluster
#subject to C6{i in 1..h}:
#sum {m in 1..n} M[i,m,1] + M[i,m,2];  <= 1;

#inposer un point comme centre
#subject to C6{i in 1..h}:
#sum {m in 1..n}  C[m,2]  = 1;
	
#minimize f: ;
#minimize f: sum {i in 1..n, j in 1..2} X[i,j]*(sum{k in 1..K}C[j,k]);
	
minimize f: sum{i in 1..h,j in 1..n,k in 1..n} M[i,j,k]*D[j,k];

solve;

display M;
display C;
display f;

data;

param n := 6;
param h := 2;
param B := 42;

param D :=
1 1 0
1 2 1
1 3 1.41
1 4 1.41
1 5 1
1 6 2.24
2 1 1
2 2 0
2 3 1
2 4 2.24
2 5 2
2 6 2
3 1 1.41
3 2 1
3 3 0
3 4 2.83
3 5 2.24
3 6 1
4 1 1.41
4 2 2.24
4 3 2.83
4 4 0
4 5 1
4 6 3.61
5 1 5
5 2 2
5 3 2.24
5 4 1
5 5 0
5 6 2.83
6 1 2.24
6 2 2
6 3 1
6 4 3.61
6 5 2.83
6 6 0;


end;
