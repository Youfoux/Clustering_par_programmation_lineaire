#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int main(int argc, char ** argv)
{
	
	FILE * fichier  = NULL;
	FILE * f_sortie =NULL;
	int fin = 0;
	
	
	if(argc > 2)
	{
		fichier = fopen(argv[1],"r");
		f_sortie = fopen(argv[2],"w");		
	}
	
	
	if((fichier != NULL) && (f_sortie != NULL))
	{
			
		char cour;
		char * pCour = malloc(sizeof(char));
		char * nombre = malloc(10*sizeof(char));
		cour = fgetc(fichier);
		int clus_cour = 1;
		int clus_nouv;
				
		fprintf(f_sortie,"cluster %d\n",clus_cour);
		fflush(f_sortie);
		
		
		
		while(cour != EOF)
		{
			
			fflush(stdout);
			fseek(fichier,6,SEEK_CUR);
			cour = fgetc(fichier);
			
			if(cour == 'M' && !fin)
			{
				fseek(fichier,1,SEEK_CUR);
				cour = fgetc(fichier);
				
				
				
				
				while(cour!=',')
				{
					
					*pCour = cour;
					strcat(nombre,pCour);					
					cour = fgetc(fichier);
					
					
				}
								
				clus_nouv = atoi(nombre);
						
				
				if(clus_cour != clus_nouv)
				{
					clus_cour = clus_nouv;
					fprintf(f_sortie,"cluster %d\n",clus_cour);
					fflush(f_sortie);
				}
				
				cour =fgetc(fichier);
				while(cour!=',')
				{
									
					cour = fgetc(fichier);					
					
				}
				cour = fgetc(fichier);
				
				
				
				free(nombre);
				nombre = malloc(10*sizeof(char));			
				
				
				while(cour!=')')
				{
					*pCour = cour;
					strcat(nombre,pCour);
					cour = fgetc(fichier);
				}	
					
				fprintf(f_sortie,"%d\n",atoi(nombre));
				fflush(f_sortie);
				free(nombre);
				nombre = malloc(10*sizeof(char));
				fseek(fichier,2,SEEK_CUR);
				
				
				
			}
			else if (cour == 'C')
			{
				fin =1;
				fseek(fichier,1,SEEK_CUR);
				cour = fgetc(fichier);
				
				
				
				
				while(cour!=',')
				{
					
					*pCour = cour;
					strcat(nombre,pCour);					
					cour = fgetc(fichier);
					
					
				}
								
				clus_nouv = atoi(nombre);
							
				
				if(clus_cour != clus_nouv)
				{
					clus_cour = clus_nouv;
					fprintf(f_sortie,"Centre du cluster %d : ",clus_cour);
					fflush(f_sortie);
				}
				
				cour =fgetc(fichier);
				
				
				
				free(nombre);
				nombre = malloc(10*sizeof(char));			
				
				
				while(cour!=')')
				{
					*pCour = cour;
					strcat(nombre,pCour);
					cour = fgetc(fichier);
				}	
					
				fprintf(f_sortie,"%d\n",atoi(nombre));
				fflush(f_sortie);
				free(nombre);
				nombre = malloc(10*sizeof(char));
				fseek(fichier,2,SEEK_CUR);		
			}		
					
		}
		fclose(fichier);
		fclose(f_sortie);
	}
	else
	{
		printf("ERREUR LORS DE L'OUVERTURE DES FICHIERS");
	}
	printf("FIN DE LA CONVERSION\n");
	return 0;		
}

