#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<readline/readline.h>
#include<readline/history.h>

// Clearing the shell using escape sequences
#define clear() printf("\r ")

int isIn(int x, int tableau[], int taille){
	int res = 0;
	int i=0;
	while (i<taille && res==0) {
		if (tableau[i]==x) {
			res=1;
		}
		i++;
	}
	return res;
}

void resetTab(int tableau[], int taille){
	int i;
	for (i=0;i<taille;i++){
		tableau[i]=-1;
	}
}

void resetTab2D(int tableau[][1000], int taille1,int taille2){
	int i;
	int j;
	for (i=0;i<taille1;i++){
		for(j = 0;j<taille2;j++)
		{
			tableau[i][j]=0;
		}
	}
}

void swap(float * a,float *b)
{
	int t = *a;
	*a = *b;
	*b = t;
}

int main(int argc, char **argv)
{
	clock_t begin = clock();	

	
	int np, nc;
	int stop;
	int totalMut = 0;
	int proba;
	int i,j,k,l;
	int iter,step;
	int c;
	int indexC;
	int index1, index2;
	int unOuDeux;
	int centreProche;
	int randomInt;
	float somme, distanceMin,distanceD;
	float * v1,v2;
	int index;
	int dim;
	char progressBar[100];
	
	
	int clus_cour;
	int centre_cour;
	
	/*Lecture des distances*/
	
	FILE* fichier = NULL;
	
	if(argc>4){
		
		np = atoi(argv[1]);
		nc = atoi(argv[2]);
		dim = atoi(argv[3]);
		
		fflush(stdout);
		fichier = fopen(argv[4], "r");
		int tableauInd[200][nc];
		int tableauNouvInd[200][nc];
		int tableauInter[200][nc];
		//int Centres[nc];
		int indNouv[nc];
		int indNouv2[nc];
		int indAlea[50];
		float tableau[np][dim];
		/*float distance[np][np];*/
		int assocPC[np];
		
		float **distance = malloc(np* sizeof *distance);
		for (i = 0; i < np; i++)
			distance[i] = malloc(np * sizeof(float));
	
		float obj[400][2];
		
		
		/*Lecture des distances dans le fichier*/
		for (i=0;i<np;i++) {
			for (j=0;j<dim;j++) {
				fscanf(fichier, "%f ", &tableau[i][j]);
			}
		}
		
		for (i=0;i<np;i++) {
			for (k=0;k<np;k++) {
				somme = 0;
				for (j=0;j<dim;j++) {
					somme += pow((tableau[i][j] - tableau[k][j]),2);
				}
				distance[i][k] = sqrt(somme);
			}
		}
		
		fclose(fichier);
		
		srand(time(NULL));
		
		/*Initialisation des individus*/	
		for (i=0;i<200;i++){
			for (k=0;k<nc;k++){				
				c = rand()%np;
				tableauInd[i][k] = c;				
			}
		}
		
		for (step=0;step<500;step++){
		    //Progress bar
		    clear();
		    if(step%5 == 0)
		    {
		        printf("%d",step/5);
		        progressBar[step/5] = '-';
		        for (l = 0; l < step/5; l++)
		        {
		            printf("%c",progressBar[l]);
		        }
		         printf(">");
		        fflush(stdout);
		    }

			//
			for (i=0;i<200;i++){
				for(j = 0;j<nc;j++)
				{
					tableauNouvInd[i][j]=-1;
				}
			}

			for (iter=0;iter<200;iter+=2){
				
				/*Selection de deux individus au hasard*/
				
				index1 = rand()%200;
				index2 = rand()%200;
				while (index1 == index2){
					index2 = rand()%200;
				}
				
				/*Croisement*/
					
				/*Creation du nouvel individu*/
				
				index = rand()%(nc-1) +1;
				
				for(i = 0; i<index;i++)
				{
					indNouv[i] = tableauInd[index1][i];
					indNouv2[i] = tableauInd[index2][i];
				}
				
				for(i = index; i<nc;i++)
				{
					indNouv2[i] = tableauInd[index1][i];
					indNouv[i] = tableauInd[index2][i];
				}
				
				/* Mutation */
				for(i = 0; i< nc; i++)
				{
					proba = rand()%100;
					if(proba > 97)
					{
						indNouv[i] = rand()%np;						
					}
					proba = rand()%100;
					if(proba > 97)
					{
						indNouv2[i] = rand()%np;						
					}
				}
				
				/* Stockage */
				
				for(i=0;i<nc;i++){
					tableauNouvInd[iter][i] = indNouv[i];
					tableauNouvInd[iter+1][i] = indNouv2[i];
				}		
				
			}		
			
			/*Calcul des fonctions objectifs*/
			for(i = 0; i< 400; i++)
			{
				if(i < 200){
					somme = 0;			
					for(j=0;j<np;j++)
					{
						distanceMin = distance[j][tableauInd[i][0]];
						for(k = 0; k<nc; k++)
						{
							distanceD = distance[j][tableauInd[i][k]];
							if(distanceD < distanceMin)
							{
								distanceMin = distanceD;
							}
						}
						somme+=distanceMin;
					}
					obj[i][0] = somme;
					obj[i][1] = i;
				}
				else
				{
					somme = 0;			
					for(j=0;j<np;j++)
					{
						distanceMin = distance[j][tableauNouvInd[i-200][0]];
						for(k = 1; k<nc; k++)
						{
							distanceD = distance[j][tableauNouvInd[i-200][k]];
							if(distanceD < distanceMin)
							{
								distanceMin = distanceD;
							}
						}
						somme+=distanceMin;
					}
					obj[i][0] = somme;
					obj[i][1] = i;
				}
			}
				
			/* Tri des fonctions objectifs */
			
			for(i=399;i>0;i--){
				for(j=1;j<=i;j++){
					if(obj[j-1][0]>obj[j][0]){							
						swap(&obj[j-1][0],&obj[j][0]);
						swap(&obj[j-1][1],&obj[j][1]);						
					}
				}
			}
					
			/*Prise des 150 meilleurs individus*/
			for (i=0;i<200;i++){
				for(j = 0;j<nc;j++)
				{
					tableauInter[i][j]=-1;
				}
			}
			
			for(i=0;i<150;i++){
				for(j=0;j<nc;j++){
					if((int)obj[i][1] <200){
						tableauInter[i][j]=tableauInd[(int)obj[i][1]][j];
					}
					else{
						tableauInter[i][j]=tableauNouvInd[(int)obj[i][1]-200][j];
					}
				}
			}
			
			/*Selection de 50 individus aleatoires*/
			
			for(i=0;i<50;i++){
				indAlea[i]=0;
			}
			
			for(i=0;i<50;i++){
				randomInt = rand()%250 + 150;
				while (isIn(randomInt,indAlea,50) == 1){
					randomInt = rand()%250 + 150;
				}
				indAlea[i]=randomInt;
				for(j=0;j<nc;j++){
					if((int)obj[randomInt][1]<200){
						tableauInter[i+150][j]=tableauInd[(int)obj[randomInt][1]][j];
					}
					else{
						tableauInter[i+150][j]=tableauNouvInd[(int)obj[randomInt][1]-200][j];
					}
				}
			}
			
			/*Copie de tableauInter dans tableauInd*/
			
			for(i=0;i<200;i++){
				for(j=0;j<nc;j++){
					tableauInd[i][j]=tableauInter[i][j];
				}
			}
		}
		/*Affichage*/
		for(j=0;j<nc;j++){
			printf("%d ",tableauInd[0][j]);
		}
		
		printf("\nobjectif %lf\n",obj[0][0]);
		
		
		clock_t end = clock();
		double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
		printf("\ntime spent %f\n", time_spent);
		
		/*Ecritage*/
		fichier = fopen(argv[5],"w");
		
		for(i=0;i<np;i++)
		{
			distanceMin = distance[i][tableauInd[0][0]];
			centreProche = 0;
			for(j=1;j<nc;j++)
			{
				distanceD = distance[i][tableauInd[0][j]];
				
				if (distanceD < distanceMin)
				{
					distanceMin = distanceD;
					centreProche = j;
				}
			}
			// Ecriture des coordonnees des points
			for(k = 0; k <dim; k++ )
			{
			    fprintf(fichier,"%f ", tableau[i][k]);
			}
			fprintf(fichier,"%d\n", tableauInd[0][centreProche]+1);
		}
		
		fclose(fichier);
		
		for (i=0;i<np;i++)
		{
			free(distance[i]);
		}
		free(distance);
		
	}
	
				
}			
			

